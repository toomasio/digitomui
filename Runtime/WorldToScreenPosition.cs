﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomUI
{
    public class WorldToScreenPosition : MonoBehaviour
    {
        private enum MonoUpdateType { Update, FixedUpdate, LateUpdate }

        [SerializeField] private Transform followTarget = null;
        [SerializeField] private Vector3 worldPositionTarget = default;
        [SerializeField] private Vector3 offset = default;
        [SerializeField] private Canvas canvas = null;
        [SerializeField] private bool keepOnCanvas = true;
        [SerializeField] private RectTransform offCanvasPosition = null;
        [SerializeField] private float heightBuffer = 0;
        [SerializeField] private float widthBuffer = 0;
        [SerializeField, Range(0f, 1f)] private float angleTolerance = 0.7f;
        [SerializeField, Range(0f, 1f)] private float angleTransitionTime = 0.7f;
        [SerializeField] private MonoUpdateType updateType = default;

        public Transform FollowTarget { get { return followTarget; } set { followTarget = value; } }
        public Vector3 WorldPositionTarget { get { return worldPositionTarget; } set { worldPositionTarget = value; } }
        public Vector3 Offset { get { return offset; } set { offset = value; } }
        public float HeightBuffer { get { return heightBuffer; } set { heightBuffer = value; } }
        public float WidthBuffer { get { return widthBuffer; } set { widthBuffer = value; } }

        private RectTransform rect;
        private RectTransform canvasRect;
        private Camera cam;
        private Vector3 inputPosition;
        private Vector3 desiredPosition;
        private Vector3 lastPos;
        private float lerpTimer;
        private bool smoothToggle;

        private void Awake()
        {
            lerpTimer = 0;
            GetComponents();
        }

        void GetComponents()
        {
            rect = GetComponent<RectTransform>();
            cam = Camera.main;
            if (canvas)
                canvasRect = canvas.GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (updateType != MonoUpdateType.Update) return;
            Tick();
        }

        private void FixedUpdate()
        {
            if (updateType != MonoUpdateType.FixedUpdate) return;
            Tick();
        }

        private void LateUpdate()
        {
            if (updateType != MonoUpdateType.LateUpdate) return;
            Tick();
        }

        void Tick()
        {
            if (!cam)return;
            inputPosition = followTarget ? followTarget.position : worldPositionTarget;
            inputPosition += offset;

            desiredPosition = cam.WorldToScreenPoint(inputPosition);
            desiredPosition.z = 0;
            rect.position = desiredPosition;

            if (keepOnCanvas)
            {
                var height = canvasRect.sizeDelta.y;
                var width = canvasRect.sizeDelta.x;
                var middle = new Vector2(width / 2, height / 2);
                var pos = rect.position;
                var dir = followTarget.position - cam.transform.position;
                var facingAngle = Vector3.Dot(cam.transform.forward, dir.normalized);

                bool behindCamera = facingAngle < angleTolerance;
                if (behindCamera)
                {
                    if (!smoothToggle)
                    {
                        lerpTimer = 0;
                        smoothToggle = true;
                    }
                        
                    dir = cam.transform.InverseTransformDirection(dir);
                    dir.z = 0;
                    pos = middle + (dir.normalized * new Vector2(width * 0.71f, height * 0.71f));
                }
                else if (smoothToggle)
                {
                    lerpTimer = 0;
                    smoothToggle = false;
                }
                    
                bool offscreenPos = pos.x < widthBuffer || pos.x > width - widthBuffer || pos.y > height - heightBuffer || pos.y < heightBuffer;

                if (offscreenPos)
                {
                    if (offCanvasPosition)
                        rect.position = offCanvasPosition.position;
                    else
                    {
                        var x = Mathf.Clamp(pos.x, widthBuffer, width - widthBuffer);
                        var y = Mathf.Clamp(pos.y, heightBuffer, height - heightBuffer);
                        var des = new Vector3(x, y, 0);
                        if (lerpTimer != angleTransitionTime)
                        {
                            lerpTimer += Time.deltaTime;
                            if (lerpTimer > angleTransitionTime)
                                lerpTimer = angleTransitionTime;
                            var perc = lerpTimer / angleTransitionTime;
                            des = Vector3.Lerp(lastPos, des, perc);
                        }
                        rect.position = des;
                    }
                }
                lastPos = rect.position;
            }
            
        }

        private void OnValidate()
        {
            lerpTimer = angleTransitionTime;
            GetComponents();
            Tick();
        }
    }
}