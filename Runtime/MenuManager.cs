﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace DigitomUI
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] private InputActionAsset inputAsset = null;
        [SerializeField] private string[] inputsToDisable = null;
        //[SerializeField] private InputActionReference submitInput = null;
        [SerializeField] private InputActionReference cancelInput = null;

        [SerializeField] private bool freezeTime = false;

        public bool IsPaused { get; private set; }

        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void OnEnable()
        {
            cancelInput.action.Enable();
            cancelInput.action.performed += OnCancelInput;
        }

        private void OnDisable()
        {
            cancelInput.action.Disable();
        }

        private void OnCancelInput(InputAction.CallbackContext ctx)
        {
            if (IsPaused) 
                UnPause();
            else
                Pause();
        }

        private void Pause()
        {
            if (freezeTime)
                Time.timeScale = 0;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            for (int i = 0; i < inputsToDisable.Length; i++)
            {
                var map = inputAsset.FindActionMap(inputsToDisable[i]);
                map.Disable();
            }

            IsPaused = true;
        }

        private void UnPause()
        {
            if (freezeTime)
                Time.timeScale = 1;

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            for (int i = 0; i < inputsToDisable.Length; i++)
            {
                var map = inputAsset.FindActionMap(inputsToDisable[i]);
                map.Enable();
            }
            IsPaused = false;
        }
    }
}


