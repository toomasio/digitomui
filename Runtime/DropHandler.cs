﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropHandler : MonoBehaviour, IDropHandler
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private GraphicRaycaster raycaster;
    private RectTransform rectTrans;

    private List<RaycastResult> cornerCastResults = new List<RaycastResult>(4);

    private void Awake()
    {
        rectTrans = GetComponent<RectTransform>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(gameObject);
    }
}
