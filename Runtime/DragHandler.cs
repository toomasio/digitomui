﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private Canvas canvas;
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private GraphicRaycaster rayCaster;
    [SerializeField] private EventSystem system;
    private RectTransform rectTrans;
    private PointerEventData[] cornerPointers = new PointerEventData[4];
    private Dictionary<int, List<RaycastResult>> cornerHits = new Dictionary<int, List<RaycastResult>>(4);

    private RaycastResult closestToMousePos;
    private float closestDistance;
    private void Awake()
    {
        rectTrans = GetComponent<RectTransform>();

        for (int i = 0; i < cornerPointers.Length; i++)
        {
            cornerPointers[i] = new PointerEventData(system);
            cornerHits.Add(i, new List<RaycastResult>());
        }

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = false;
        rectTrans.transform.position = eventData.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTrans.position += (Vector3)eventData.delta;
        DoCornerCast();

        closestToMousePos = default;
        closestDistance = Mathf.Infinity;
        for (int i = 0; i < cornerHits.Count; i++)
        {
            var hit = cornerHits[i];
            for (int j = 0; j < hit.Count; j++)
            {
                var go = hit[j].gameObject;
                var d = Vector2.Distance(go.transform.position, eventData.position);
                if (d < closestDistance)
                {
                    closestToMousePos = hit[j];
                    closestDistance = d;
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        if (eventData.pointerEnter) return;

        if (!closestToMousePos.Equals((RaycastResult)default))
        {
            var drops = closestToMousePos.gameObject.GetComponents<IDropHandler>();
            for (int i = 0; i < drops.Length; i++)
                drops[i].OnDrop(eventData);
        }
    }

    protected virtual void DoCornerCast()
    {
        var corners = new Vector3[4];
        rectTrans.GetWorldCorners(corners);

        for (int i = 0; i < corners.Length; i++)
        {
            cornerPointers[i].position = corners[i];
            system.RaycastAll(cornerPointers[i], cornerHits[i]);
        }

    }

}
